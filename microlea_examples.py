'''
------------------------------------------------------------------------------
Examples using MicroLea
------------------------------------------------------------------------------
'''

from microlea import *

# aliases defined for convenience
E = ElemPex
EB = ElemPex.bool
F = FuncPex
T = TuplePex
C = CondPex
B = TablePex
M = MixtPex
(t,f) = (True,False)

# ----------------------------------------------------------------------------
# elementary pex and functional pex (operators)
# ----------------------------------------------------------------------------
# fair die
da = E({1: 1/6, 2: 1/6, 3: 1/6, 4: 1/6, 5: 1/6, 6: 1/6})
print (da)
# -> {1: 0.16666666666666669, 2: 0.16666666666666669, 3: 0.16666666666666669, 4: 0.16666666666666669, 5: 0.16666666666666669, 6: 0.16666666666666669}
# note: thanks to condensation, this is equivalent to
# da = E({1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1})
# comparison operators create a boolean distribution...
print (da == 1)
# -> {False: 0.8333333333333334, True: 0.16666666666666669}
# ...from which the p function extracts the probability of true
print (P(da == 1))
# -> 0.16666666666666669
# inequality operators are supported
print (P(da >= 4))
# -> 0.5000000000000001
print (P(da >= 7))
# -> 0.0
# binding in action: da occurrences in the same pex are consistent
print(da+da)
# -> {2: 0.16666666666666669, 4: 0.16666666666666669, 6: 0.16666666666666669, 8: 0.16666666666666669, 10: 0.16666666666666669, 12: 0.16666666666666669}
print(da-da)
# -> {0: 1.0}
# boolean operator AND is supported, with binding
print (P((2 <= da) & (da < 6)))
# -> 0.6666666666666667
# as well as OR
print (P((2 > da) | (da >= 6)))
# -> 0.33333333333333337
# and NOT (here we can check de Morgan's law)
print (P(~((2 > da) | (da >= 6))))
# -> 0.6666666666666667
# create a new die, by cloning
db = da.new()
# add the results of the two dice
dab = da + db
print (dab)
# modulo operator to get parity bit of each die and of two dice together
parity_da = da % 2
parity_db = db % 2
parity_dab = dab % 2
print (parity_dab)
# -> {0: 0.5, 1: 0.5}
# applying a user-defined function
def parityNameFunc(b):
    if b == 0:
        return 'even'
    return 'odd'
parityName_dab = F(parityNameFunc,parity_dab)
print (parityName_dab)
# -> {'odd': 0.5, 'even': 0.5}

# ----------------------------------------------------------------------------
# tuple pex
# ----------------------------------------------------------------------------
# getting the 36 combinations of 2 dice results
print (T(da,db))
# -> {(6, 4): 0.027777777777777773, (3, 2): 0.027777777777777773, (1, 3): 0.027777777777777773, ... }
# show binding in action: only 6 combinations because the same die da is repeated
print (T(da,da))
# -> {(6, 6): 0.16666666666666669, (5, 5): 0.16666666666666669, (2, 2): 0.16666666666666669, (4, 4): 0.16666666666666669, (1, 1): 0.16666666666666669, (3, 3): 0.16666666666666669}
# show binding in action: in each of 36 inner tuples, the third element is equal to the sum of the first two
print (T(da,db,dab))
# -> {(2, 3, 5): 0.027777777777777773, (2, 4, 6): 0.027777777777777773, (5, 1, 6): 0.027777777777777773, (3, 4, 7): 0.027777777777777773, ... }

# ----------------------------------------------------------------------------
# conditional pex
# ----------------------------------------------------------------------------
# conditional pex: die da given that we know that da < 3
print (C(da,da < 3))
# -> {1: 0.5, 2: 0.5}
# equivalent to this
print (da.given(da < 3))
# -> {1: 0.5, 2: 0.5}
# sum of dice given that we know that da < 3
print (dab.given(da < 3))
# -> {2: 0.08333333333333331, 3: 0.16666666666666663, 4: 0.16666666666666663, 5: 0.16666666666666663, 6: 0.16666666666666663, 7: 0.16666666666666663, 8: 0.08333333333333331}
# boolean AND: sum of dice given that we know that da < 3 AND db < 3
print (dab.given((da < 3) & (db < 3)))
# -> {2: 0.25, 3: 0.5, 4: 0.25}
# boolean OR: sum of dice given that we know that da < 3 OR db < 3
print (dab.given((da < 3) | (db < 3)))
# -> {2: 0.04999999999999999, 3: 0.09999999999999998, 4: 0.14999999999999997, 5: 0.19999999999999996, 6: 0.19999999999999996, 7: 0.19999999999999996, 8: 0.09999999999999998}
# conditonal pex can also derive input from evidence on output
print (da.given(dab < 4))
# -> {1: 0.6666666666666666, 2: 0.3333333333333333}
# tuple pex is useful to have explanation, by getting elementary events
print (T(da,db,dab).given(dab < 4))
# -> {(1, 1, 2): 0.3333333333333333, (2, 1, 3): 0.3333333333333333, (1, 2, 3): 0.3333333333333333}
# conjunction of conditions
print (T(da,db,dab).given((da < 3) & (db < 3)))
# -> {(1, 1, 2): 0.25, (2, 2, 4): 0.25, (2, 1, 3): 0.25, (1, 2, 3): 0.25}
# equivalent to multi-conditional, which is more efficient
print (CondMultPex(T(da,db,dab),(da < 3, db < 3)))
# -> {(1, 1, 2): 0.25, (2, 2, 4): 0.25, (2, 1, 3): 0.25, (1, 2, 3): 0.25}
# or, equivalently,
print (T(da,db,dab).givenMult(da < 3, db < 3))
# -> {(1, 1, 2): 0.25, (2, 2, 4): 0.25, (2, 1, 3): 0.25, (1, 2, 3): 0.25}
print (parityName_dab.given(parity_da == parity_db))
# -> {'even': 1.0}
print (parityName_dab.given(parity_da != parity_db))
# -> {'odd': 1.0}
# conditional probability
print (P((dab > 4).given(da < 3)))
# -> 0.5833333333333333
# verification of conditional probability definition: P(A | B) = P(A & B) / P(B)
print (P((dab > 4) & (da < 3)) / P(da < 3))
# -> 0.5833333333333331
# verification of Bayes rule: P(A | B) = P(B | A) * P(A) / P(B)
print (P((da < 3).given(dab > 4)) * P(dab > 4) / P(da < 3))
# -> 0.5833333333333331
# same checks for the counterpart P(B | A)
print (P((da < 3).given(dab > 4)))
# -> 0.2333333333333333
print (P((da < 3) & (dab > 4)) / P(dab > 4))
# -> 0.23333333333333328
print (P((dab > 4).given(da < 3)) * P(da < 3) / P(dab > 4))
# -> 0.23333333333333334


# ----------------------------------------------------------------------------
# table pex and Bayesian networks
# ----------------------------------------------------------------------------
# prior probability
# same as rain = E({True:0.20,False:0.80})
rain = EB(0.20)
# 1. Modelling CPT using table pex and tuple pex
# sprinkler as a CPT depending of rain
sprinkler = B( rain, { True  : EB(0.01),
                       False : EB(0.40)} )
# grass_wet as a CPT depending of sprinkler and rain
grass_wet = B( T(sprinkler, rain ),
              { (False    , False) : False,
                (False    , True ) : EB(0.80),
                (True     , False) : EB(0.90),
                (True     , True ) : EB(0.99)} )
# check rain definition
print (P(rain))
# -> 0.2
# check sprinkler CPT definition
print (P(sprinkler.given(rain)))
# -> 0.01
print (P(sprinkler.given(~rain)))
# -> 0.4000000000000001
# check grass_wet CPT definition
print (P(grass_wet.given(~sprinkler & ~rain)))
# -> 0.0
print (P(grass_wet.given(~sprinkler & rain)))
# -> 0.8
print (P(grass_wet.given( sprinkler &~rain)))
# -> 0.9000000000000001
print (P(grass_wet.given( sprinkler & rain)))
# -> 0.99
# unconditional probabilities
print (P(sprinkler))
# -> 0.32200000000000006
print (P(grass_wet))
# -> 0.4483800000000001
print (P(grass_wet.given(rain)))
# -> 0.8019000000000001
print (P(rain.given(grass_wet)))
# -> 0.35768767563227616
print (P(rain.given(~grass_wet)))
# -> 0.07182480693230847
# calculate full joint distribution using tuple pex (8 entries)
print(T(rain,sprinkler,grass_wet))
# -> {(False, True, True): 0.28800000000000003, (False, False, False): 0.48, (True, True, False): 2.000000000000002e-05, (True, False, False): 0.039599999999999996, (False, True, False): 0.031999999999999994, (True, True, True): 0.0019800000000000004, (True, False, True): 0.1584}

# extend the BN with numerical pex
measure = B( grass_wet, { False : E({0: 1/2, 1: 3/8, 2: 1/8 }),
                          True  : E({2: 1/8, 3: 3/8, 4: 1/2 })})
print(measure.given(~grass_wet))
# -> {0: 0.4999999999999999, 1: 0.37499999999999994, 2: 0.12499999999999997}
print(measure.given(grass_wet))
# -> {2: 0.12500000000000003, 3: 0.37500000000000006, 4: 0.5000000000000001}
print(measure)
# -> {0: 0.27580999999999994, 1: 0.20685749999999994, 2: 0.12499999999999997, 3: 0.16814250000000003, 4: 0.22419}
print(measure.given(rain))
# -> {0: 0.09904999999999999, 1: 0.07428749999999998, 2: 0.125, 3: 0.30071250000000005, 4: 0.40095000000000003}
print(measure.given(~rain))
# -> {0: 0.32, 1: 0.24, 2: 0.125, 3: 0.135, 4: 0.18000000000000002}
print(rain.given(measure==4))
# -> {False: 0.6423123243677239, True: 0.35768767563227616}
print(rain.given(measure>=2))
# -> {False: 0.6804134671608686, True: 0.31958653283913147}

# ----------------------------------------------------------------------------
# table pex and Bayesian networks (cascaded version)
# ----------------------------------------------------------------------------
# 2. Modelling CPT using cascaded table pex
grass_wet2 = B( sprinkler,
                 { False : B( rain, { False : False,
                                      True  : EB(0.80) }),
                   True  : B( rain, { False : EB(0.90),
                                      True  : EB(0.99) })} )
print (P(grass_wet2.given(rain)))
# -> 0.8019000000000001
print (P(rain.given(grass_wet2)))
# -> 0.35768767563227616
print (P(rain.given(~grass_wet2)))
# -> 0.07182480693230847

# ----------------------------------------------------------------------------
# mixture pex and Bayesian networks
# ----------------------------------------------------------------------------
# 3. Modelling CPT using mixture pex and conditional pex
sprinkler3 = M( C(EB(0.01),  rain),
                C(EB(0.40), ~rain))
grass_wet3 = M( C(False   , ~sprinkler3 & ~rain ),
                C(EB(0.80), ~sprinkler3 &  rain ),
                C(EB(0.90),  sprinkler3 & ~rain ),
                C(EB(0.99),  sprinkler3 &  rain ))
print (P(grass_wet3.given(rain)))
# -> 0.8019000000000001
print (P(rain.given(grass_wet3)))
# -> 0.35768767563227616
print (P(rain.given(~grass_wet3)))
# -> 0.07182480693230847


# ----------------------------------------------------------------------------
# probabilities as fractions
# ----------------------------------------------------------------------------
from fractions import Fraction
u = Fraction(1)
df = E(dict((i,u) for i in range(1,7)))
print (df)
dg = df.new()
dfg = df + dg
print (dfg)
rain4 = EB(u*20/100)
sprinkler4 = B( rain4, { True  : EB(u*1/100),
                         False : EB(u*40/100)} )
# grass_wet as a CPT depending of sprinkler and rain
grass_wet4 = B( T(sprinkler4, rain4 ),
               { (False     , False) : False,
                 (False     , True ) : EB(u*80/100),
                 (True      , False) : EB(u*90/100),
                 (True      , True ) : EB(u*99/100)} )
print (P(grass_wet4.given(rain4)))
# -> 8019/10000
print (float(P(grass_wet4.given(rain4))))
# -> 0.8019
print (P(rain4.given(grass_wet4)))
# -> 891/2491
print (float(P(rain4.given(grass_wet4))))
# -> 0.3576876756322762
print (P(rain4.given(~grass_wet4)))
# -> 1981/27581
print (float(P(rain4.given(~grass_wet4))))
# -> 0.07182480693230847

'''
# performance test of multiple conditional pex
print (0)
for _ in range(200000):
    db.given((da < 2) & (db < 6) & (dab == 2))
print (1)
for _ in range(200000):
    db.givenMult(da < 2, db < 6, dab == 2)
print (2)
'''