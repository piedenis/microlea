'''
MIT License

Copyright (c) 2017 Pierre Denis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''
'''
------------------------------------------------------------------------------
microlea.py

author:   Pierre Denis
requires: Python 3

The microlea module defines classes for defining p-expressions (pex) allowing
to model discrete probability distributions.

Microlea is a light re-implementation of lea module, with the objective of
showing as clearly as possible the Python implementation of the Statues
algorithm, a probabilistic inference algorithm based on generators.

The modules defines eight classes:
- Pex is the abstract base class for all types of pex
- ElemPex defines an elementary pex
- TuplePex defines an tuple pex
- FuncPex defines a functional pex
- CondPex defines a conditional pex
- CondMultPex defines a multi-conditional pex
- TablePex defines a table pex
- MixtPex defines a mixture pex

Pex defines the genAtoms method, it calls the genAtomesBytype hook method,
which is implemented in each Pex concrete subclass (see template method design
pattern). Both methods are generators and are mutually recursive.
------------------------------------------------------------------------------
'''

import operator
from collections import defaultdict

class Pex(object):
    ''' abstract base class representing a discrete probability expresssion
        (pex), which can be evaluated as a probability mass function (pmf)
        [equivalent in Lea module: Lea class]
    '''

    class Error(Exception):
        ''' exception raised in case of error in the definition of a pex
        '''
        pass

    __slots__ = ('_val',)

    def __init__(self):
        ''' initializes a new instance of Pex
        '''
        # self is used as a dummy value to express that no value is currently
        # bound; note that None is not a good dummy value since it prevents
        # using None as value in a distribution
        self._val = self

    def marg(self):
        ''' evaluates the PMF of self and returns it as a dictionary {v: P(v)}
        '''
        vpsDict = defaultdict(int,{})
        sp = 0
        for (v,p) in self.genAtoms():
            sp += p
            vpsDict[v] += p
        if sp == 0:
            raise Pex.Error('no value (may be due to unfeasible given condition)')
        return dict((v,p/sp) for (v,p) in vpsDict.items())

    def calcP(self,val):
        ''' evaluates and returns the probability of val
            equivalent to self.marg()[val] but more efficient
        '''
        sp = 0
        pr = 0
        for (v,p) in self.genAtoms():
            sp += p
            if v == val:
                pr += p
        if sp == 0:
            raise Pex.Error('no value (may be due to unfeasible given condition)')
        return pr / sp

    def genAtoms(self):
        ''' generates independent pairs (v,P(v)), performing binding of values
            and taking into account current bindings
        '''
        if self._val is not self:
            # self is bound, yield the bound value with probability 1
            yield (self._val,1)
        else:
            # self is not yet bound
            try:
                # dispatch to generator method implemented in each Pex subclass
                for (v,p) in self.genAtomsByType():
                    # bind self to value v
                    self._val = v
                    # yield value v with its probability p
                    yield (v,p)
            finally:
                # unbind self
                self._val = self

    def __str__(self):
        ''' evaluates the PMF of self and returns it as a string using Python
            dictionary notation, rounding the probability decimals to 4 digits;
            the support values are sorted, whenever possible
        '''
        vps = self.marg().items()
        try:
            vps = sorted(vps)
        except:
            pass
        return '{%s}' % (', '.join('%s: %6.4f'%vp for vp in vps))

    # the following assignment allows displaying Pex' PMF as defined in
    # __str__, without needing to call Python's print function
    __repr__ = __str__

    def given(self,evidencePex):
        ''' returns a new instance of CondPex representing the conditional pex
            of self pex given that evidencePex is true
        '''
        return CondPex(self,evidencePex)

    def givenMult(self,*evidencePexes):
        ''' returns a new instance of CondPex representing the conditional pex
            of self pex given that evidencePex is true
        '''
        return CondMultPex(self,evidencePexes)

    def switch(self,pexDict):
        ''' returns a new instance of TablePex representing the table pex
            defined by pexDict dictionary {k: v} where k is a value of self
            and v is the pex associated to k.
        '''
        return TablePex(self,pexDict)

    def new(self):
        ''' evaluates the PMF of self and returns it as a new ElemPex
            instance
        '''
        return ElemPex(self.marg())

    @staticmethod
    def coerce(val):
        ''' static method, returns an instance of Pex corresponding to val
            if val is a Pex, then it is returned,
            otherwise an ElemPex is returned with the single value v
            (probability equel to 1)
        '''
        if not isinstance(val,Pex):
            val = ElemPex({val:1})
        return val

    # operator overloading for Pex objects

    def __coerceFuncN(f):
        ''' returns a method that returns FuncPex representing a functional
            pex applying given function f to a TuplePex with its arguments
            (helper function used internally to do operator overloading
             of unary operators and binary 'forward' operators __xxx__)
        '''
        return lambda *x: FuncPex(f,TuplePex(*x))

    def __coerceFuncI(f):
        ''' returns a method that returns FuncPex representing a functional
            pex applying given function f to a TuplePex with its 2 arguments
            permuted
            (helper function used internally to do operator overloading
             of binary 'reverse' operators methods __rxxx__)
        '''
        return lambda a,b: FuncPex(f,TuplePex(b,a))

    # overloading of arithmetic operators and mathematical functions
    __pos__       = __coerceFuncN(operator.pos)
    __neg__       = __coerceFuncN(operator.neg)
    __add__       = __coerceFuncN(operator.add)
    __radd__      = __coerceFuncI(operator.add)
    __sub__       = __coerceFuncN(operator.sub)
    __rsub__      = __coerceFuncI(operator.sub)
    __mul__       = __coerceFuncN(operator.mul)
    __rmul__      = __coerceFuncI(operator.mul)
    __truediv__   = __coerceFuncN(operator.truediv)
    __rtruediv__  = __coerceFuncI(operator.truediv)
    __floordiv__  = __coerceFuncN(operator.floordiv)
    __rfloordiv__ = __coerceFuncI(operator.floordiv)
    __mod__       = __coerceFuncN(operator.mod)
    __rmod__      = __coerceFuncI(operator.mod)
    __pow__       = __coerceFuncN(operator.pow)
    __rpow__      = __coerceFuncI(operator.pow)
    __abs__       = __coerceFuncN(abs)

    # overloading of comparison operators
    __lt__        = __coerceFuncN(operator.lt)
    __le__        = __coerceFuncN(operator.le)
    __eq__        = __coerceFuncN(operator.eq)
    __ne__        = __coerceFuncN(operator.ne)
    __gt__        = __coerceFuncN(operator.gt)
    __ge__        = __coerceFuncN(operator.ge)

    # overloading of slicing operator
    __getitem__   = __coerceFuncN(operator.getitem)

    # overloading of bitwise operators to emulate boolean operators
    __invert__    = __coerceFuncN(operator.not_)
    __and__       = __coerceFuncN(operator.and_)
    __rand__      = __coerceFuncI(operator.and_)
    __or__        = __coerceFuncN(operator.or_)
    __ror__       = __coerceFuncI(operator.or_)
    __xor__       = __coerceFuncN(operator.xor)
    __rxor__      = __coerceFuncI(operator.xor)


class ElemPex(Pex):
    ''' subclass of Pex representing an elementary pex defined by a given pmf
        with prior probabilities
        [equivalent in Lea module: Alea class]
    '''

    __slots__ = ('_pmfDict',)

    def __init__(self,pmfDict):
        ''' initializes a new instance of ElemPex with the given pmfDict
            dictionary {k: P(k)} where each key k is associated to the
            probability of this key P(k)
        '''
        Pex.__init__(self)
        self._pmfDict = pmfDict

    @staticmethod
    def bool(p):
        ''' static method, returns a new instance of ElemPex where the
            probability of True is p and the probability of False is 1-p
        '''
        return ElemPex({True:p,False:1-p})

    def genAtomsByType(self):
        ''' generates independent pairs (v,P(v)) as defined in the given pmf
        '''
        for (v,p) in self._pmfDict.items():
            yield (v,p)


class TuplePex(Pex):
    ''' subclass of Pex representing a tuple pex defined by a head (pex) and
        a tail (tuple pex);
        it performs a cartesian product of all inner pexes, taking their
        bindings into account
        [equivalent in Lea module: Clea class]
    '''

    __slots__ = ('_headPex','_tailPexes')

    def __init__(self,*pexes):
        ''' initializes a new instance of TuplePex for the given pexes arguments
        '''
        Pex.__init__(self)
        if len(pexes) == 0:
            raise Pex.Error('TuplePex cannot be empty; use ElemPex.emptyTuple instead')
        (headPex,*tailPexes) = pexes
        self._headPex = Pex.coerce(headPex)
        if len(tailPexes) == 0:
            self._tailPexes = ElemPex.emptyTuple
        else:
            self._tailPexes = TuplePex(*tailPexes)

    def genAtomsByType(self):
        ''' generates independent pairs (v,P(v)) taking into account current
            bindings and tuple pex semantic
        '''
        for (vh,ph) in self._headPex.genAtoms():
            for (vt,pt) in self._tailPexes.genAtoms():
                yield ((vh,)+vt,ph*pt)

    @staticmethod
    def coerce(val):
        ''' static method, returns an instance of Pex corresponding to val
            if val is a Pex, then it is returned,
            otherwise an ElemPex is returned with the single value v
            (probability equel to 1)
        '''
        if not isinstance(val,TuplePex):
            val = TuplePex(val)
        return val


class FuncPex(Pex):
    ''' subclass of Pex representing a functional pex defined by a function
        and its pex argument;
        note: n-ary functions with n > 1 use a TuplePex for argument
        [equivalent in Lea module: Flea, Flea1, FLea2 classes]
    '''

    __slots__ = ('_func','_argTuplePex')

    def __init__(self,func,argTuplePex):
        ''' initializes a new instance of FuncPex for the given function func
            and the given argument argPex
        '''
        Pex.__init__(self)
        self._func = func
        self._argTuplePex = TuplePex.coerce(argTuplePex)

    def genAtomsByType(self):
        ''' generates independent pairs (v,P(v)) taking into account current
            bindings and functional pex semantic
        '''
        func = self._func
        for (v,p) in self._argTuplePex.genAtoms():
            yield (func(*v),p)


class CondPex(Pex):
    ''' subclass of Pex representing a conditional pex defined by a given
        prior pex under the condition that a given evidence pex is true
        [equivalent in Lea module: Ilea class with only one evidence argument]
    '''

    __slots__ = ('_priorPex','_evidencePex')

    def __init__(self,priorPex,evidencePex):
        ''' initializes a new instance of CondPex for the given priorPex
            and given evidencePex
        '''
        Pex.__init__(self)
        self._priorPex = Pex.coerce(priorPex)
        self._evidencePex = Pex.coerce(evidencePex)

    def genAtomsByType(self):
        ''' generates independent pairs (v,P(v)) taking into account current
            bindings and conditional pex semantic
        '''
        for (ve,pe) in self._evidencePex.genAtoms():
            if ve:
                for (v,p) in self._priorPex.genAtoms():
                    yield (v,pe*p)


class CondMultPex(Pex):
    ''' subclass of Pex representing a multi-conditional pex defined by a
        given prior pex under the condition that the conjunction of given
        evidence pexes is true;
        this uses a short-circuit evaluation: the search is halted as soon as
        one of the condition is bounded to False; hence, tnaks to the pruning,
        the present class is usually more efficient than CondPex when the
        condition is a conjunction
        [equivalent in Lea module: Ilea class]
    '''

    __slots__ = ('_priorPex','_evidencePexes')

    def __init__(self,priorPex,evidencePexes):
        ''' initializes a new instance of CondPex for the given priorPex
            and given evidencePexes (iterable)
        '''
        Pex.__init__(self)
        self._priorPex = Pex.coerce(priorPex)
        self._evidencePexes = tuple(Pex.coerce(evPex) for evPex in evidencePexes)

    @staticmethod
    def _genTrueProb(evidencePexes):
        ''' generates probabilities of True for ANDing the given evidencePexes
            (iterable) doing binding in-the-fly;
            this uses short-circuit evaluation
        '''
        if len(evidencePexes) == 0:
            yield 1
        else:
            (headEvidencePex,*tailEvidencePexes) = evidencePexes
            for (vh,ph) in headEvidencePex.genAtoms():
                if vh:
                    for pt in CondMultPex._genTrueProb(tailEvidencePexes):
                        yield ph*pt

    def genAtomsByType(self):
        ''' generates independent pairs (v,P(v)) taking into account current
            bindings and multi-conditional pex semantic
        '''
        for pe in CondMultPex._genTrueProb(self._evidencePexes):
            for (v,p) in self._priorPex.genAtoms():
                yield (v,pe*p)


class TablePex(Pex):
    ''' subclass of Pex representing a table pex defined by a lead pex and a
        dictionary associating each value of lead pex to another pex;
        TablePex may be used to define conditional probability table (CPT);
        note: for CPT depending on multiple pexes, the leadPex shall be a
        TuplePex and the dictionary keys shall be tuples;
        see also MixtPex for another way to define CPT
        [equivalent in Lea module: Tlea class]
    '''

    __slots__ = ('_leadPex','_pexDict')

    def __init__(self,leadPex,pexDict):
        ''' initializes a new instance of Pex fo the given leadPex and pexDict
            it is assumed that all values of LeadPex are present as keys in
            pexDict (if not, genAtomsByType shall raise a KeyError exception)
        '''
        Pex.__init__(self)
        self._leadPex = Pex.coerce(leadPex)
        self._pexDict = dict((v,Pex.coerce(x)) for (v,x) in pexDict.items())

    def genAtomsByType(self):
        ''' generates independent pairs (v,P(v)) taking into account current
            bindings and table pex semantic
        '''
        for (vc,pc) in self._leadPex.genAtoms():
            for (v,p) in self._pexDict[vc].genAtoms():
                yield (v,pc*p)


class MixtPex(Pex):
    ''' subclass of Pex representing a mixture pex, defined by a set of pexes
        for which the values are mixed together;
        MixtPex combined with CondPex offers an alternative way to TablePex
        for representing conditional probability table (CPT):
          TablePex( c, {c1:x1, ... ,cn:xn} )
        is logically equivalent to
          MixtPex( CondPex(x1,c==c1), ... , CondPex(xn,c==cn)))
        MixtPex approach is usually less efficient than TablePex except if
        enough table entries can be merged together due to contextual
        independence
        [equivalent in Lea module: Blea class]
    '''

    __slots__ = ('_pexes',)

    def __init__(self,*pexes):
        ''' initializes a new instance of Pex
            note that the position of pexes in the sequence is irrelevant
        '''
        Pex.__init__(self)
        self._pexes = tuple(Pex.coerce(x) for x in pexes)

    def genAtomsByType(self):
        ''' generates independent pairs (v,P(v)) taking into account current
            bindings and mixture pex semantic
        '''
        for x in self._pexes:
            for (v,p) in x.genAtoms():
                yield (v,p)


# elementary pex with empty tuple, built once for all
# (used in TuplePex constructor)
ElemPex.emptyTuple = Pex.coerce(())

def P(pex):
    ''' returns, after evaluation of pmf, the probability that the given pex
        is true; if this pex does not contains True, then 0 is returned
    '''
    ## equivalent but more efficient than pex.marg().get(True,0)
    return pex.calcP(True)
